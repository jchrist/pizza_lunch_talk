from PyQt5 import QtWidgets, QtCore, QtGui
import sys


class AllTheWidgets(QtWidgets.QWidget):
    def __init__(self, *args, **kwargs):

        super(AllTheWidgets, self).__init__(*args, **kwargs)
        v_layout = QtWidgets.QVBoxLayout(self)

        # QLabel
        label = QtWidgets.QLabel('I\'m a Label!')
        v_layout.addWidget(label)

        # QLineEdit
        line_edit = QtWidgets.QLineEdit('Edit Me!')
        v_layout.addWidget(line_edit)

        # QPushButton
        push_button = QtWidgets.QPushButton('Push Me!')
        v_layout.addWidget(push_button)

        # QSpinBox
        spin_box = QtWidgets.QSpinBox()
        v_layout.addWidget(spin_box)

        # QDoubleSpinBox
        double_spin_box = QtWidgets.QDoubleSpinBox()
        v_layout.addWidget(double_spin_box)

        # QComboBox
        combo_box = QtWidgets.QComboBox()
        combo_box.addItems(['Thing 1', 'Thing 2', 'Thing 3'])
        v_layout.addWidget(combo_box)

        # QCheckBox
        check_box = QtWidgets.QCheckBox('Check Me!')
        v_layout.addWidget(check_box)

        # QRadioButtons
        temp_h_layout = QtWidgets.QHBoxLayout()
        radios = [QtWidgets.QRadioButton(str(i)) for i in range(10)]
        [temp_h_layout.addWidget(radio) for radio in radios]
        v_layout.addLayout(temp_h_layout)

        # QButtonGroup - Must be self. otherwise it is deleted
        self.button_group = QtWidgets.QButtonGroup()
        self.button_group.addButton(check_box)
        [self.button_group.addButton(radio) for radio in radios]
        self.button_group.setExclusive(True)

        # QProgressBar
        self.progress_bar = QtWidgets.QProgressBar()
        v_layout.addWidget(self.progress_bar)

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.progress)
        self.timer.start(100)

        # Base file path
        temp_h_layout = QtWidgets.QHBoxLayout()
        # Creating line edit for the default path
        file_path_button = QtWidgets.QPushButton()
        file_path_button.setIcon(QtGui.QIcon.fromTheme('folder-open'))
        file_path_button.clicked.connect(self.select_directory)
        self.base_path_edit = QtWidgets.QLineEdit()
        self.base_path_edit.setEnabled(False)
        self.base_path_edit.setStyleSheet('color: black; '
                                          'background-color: white')
        temp_h_layout.addWidget(self.base_path_edit)
        temp_h_layout.addWidget(file_path_button)
        v_layout.addLayout(temp_h_layout)

        # Dialog Button
        button = QtWidgets.QPushButton('Display Dialog')
        button.clicked.connect(self.display_dialog)
        v_layout.addWidget(button)

        # Dialog Button
        button = QtWidgets.QPushButton('Input Password')
        button.clicked.connect(self.password)
        v_layout.addWidget(button)

    def progress(self):
        progress = self.progress_bar.value() + 1
        self.progress_bar.setValue(progress if progress < 100 else 0)

    def select_directory(self):
        openfile = QtWidgets.QFileDialog.getExistingDirectory()
        # QtWidgets.QFileDialog.getOpenFileName()
        # QtWidgets.QFileDialog.getSaveFileName()
        if not openfile:
            return
        openfile = str(QtCore.QDir.toNativeSeparators(openfile))
        self.base_path_edit.setText(openfile)

    def display_dialog(self):
        msg_box = QtWidgets.QMessageBox()
        msg_box.setText('Is this a message box?')
        msg_box.setStandardButtons(QtWidgets.QMessageBox.Yes |
                                   QtWidgets.QMessageBox.No)
        msg_box.setDefaultButton(QtWidgets.QMessageBox.Yes)
        ret = msg_box.exec_()
        if ret == QtWidgets.QMessageBox.No:
            msg_box = QtWidgets.QMessageBox()
            msg_box.setText('WRONG!!')
            msg_box.exec_()
        elif ret == QtWidgets.QMessageBox.Yes:
            msg_box = QtWidgets.QMessageBox()
            msg_box.setText('RIGHT!!')
            msg_box.exec_()

    def password(self):
        password, ok_pressed = QtWidgets.QInputDialog.getText(
            self, 'Input Password', 'Password: ', QtWidgets.QLineEdit.Password)

        if ok_pressed:
            print('hello')
            msg_box = QtWidgets.QMessageBox()
            msg_box.setText('Your password was {}. Hacking commencing'.format(password))
            msg_box.exec_()


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    widget = AllTheWidgets()
    widget.show()

    def custom_except_hook(t, val, tb):
        import traceback
        trace = ''.join(traceback.format_exception(t, val, tb))
        print(trace)
        QtCore.qFatal(str(val))

    sys.excepthook = custom_except_hook
    sys.exit(app.exec_())