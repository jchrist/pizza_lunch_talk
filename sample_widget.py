from PyQt5 import QtWidgets, QtCore, QtGui
import sys


class SampleWidget(QtWidgets.QWidget):
    def __init__(self, **kwargs):

        super().__init__()

        # label = QtWidgets.QLabel('Hello World!!')

        # label = QtWidgets.QLabel('Hello World!!', self)

        # label = QtWidgets.QLabel('Hello World!!', self)
        # label_2 = QtWidgets.QLabel('Another Label', self)

        layout = QtWidgets.QVBoxLayout()  # or QtWidget.QHBoxLayout(self)
        label = QtWidgets.QLabel('Hello World!!')
        label_2 = QtWidgets.QLabel('Another Label')

        layout.addWidget(label)
        layout.addStretch(3)
        layout.addWidget(label_2)
        self.setLayout(layout)

        # layout = QtWidgets.QHBoxLayout()
        # label = QtWidgets.QLabel('Hello World!!')
        # label_2 = QtWidgets.QLabel('Another Label')
        #
        # layout.addWidget(label, stretch=0, alignment=QtCore.Qt.AlignRight)
        # layout.addWidget(label_2, stretch=3, alignment=QtCore.Qt.AlignCenter)
        # self.setLayout(layout)

        # Set the icon for the main window
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("images/atom.png"),
                       QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.setWindowIcon(icon)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    widget = SampleWidget()
    widget.show()

    def custom_except_hook(t, val, tb):
        import traceback
        trace = ''.join(traceback.format_exception(t, val, tb))
        print(trace)
        QtCore.qFatal(str(val))

    sys.excepthook = custom_except_hook
    sys.exit(app.exec_())