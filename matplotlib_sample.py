from PyQt5 import QtGui, QtCore, QtWidgets
import numpy as np
import pyqtgraph as pg
from pyqtgraph.ptime import time
import sys
from matplotlib import pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas


class SampleWidget(QtWidgets.QWidget):
    def __init__(self, **kwargs):

        super().__init__()
        layout = QtWidgets.QHBoxLayout(self)

        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)
        self.canvas.draw()
        layout.addWidget(self.canvas)

        self.data = np.random.normal(size=(50, 5000))
        self.ptr = 0
        self.lastTime = time()
        self.fps = None

        self.ax = plt.axes()
        self.ax.set_ylim(-10, 10)
        self.curve = self.ax.plot(self.data[self.ptr % 10])[0]

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.update)
        self.timer.start(0)

    def update(self):
        # self.ax.cla()
        # self.ax.plot(self.data[self.ptr % 10])
        # self.ax.set_ylim(-10, 10)
        self.curve.set_ydata(self.data[self.ptr % 10])
        self.canvas.draw()
        self.canvas.flush_events()
        self.ptr += 1
        now = time()
        dt = now - self.lastTime
        self.lastTime = now
        if self.fps is None:
            self.fps = 1.0 / dt
        else:
            s = np.clip(dt * 3., 0, 1)
            self.fps = self.fps * (1 - s) + (1.0 / dt) * s
        self.figure.suptitle('%0.2f fps' % self.fps)
        # app.processEvents()  ## force complete redraw for every plot


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    widget = SampleWidget()
    widget.show()

    def custom_except_hook(t, val, tb):
        import traceback
        trace = ''.join(traceback.format_exception(t, val, tb))
        print(trace)
        QtCore.qFatal(str(val))

    sys.excepthook = custom_except_hook
    sys.exit(app.exec_())
