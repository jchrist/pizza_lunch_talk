from PyQt5 import QtWidgets, QtCore, QtGui
import sys


class CenteringWidget(QtWidgets.QWidget):
    def __init__(self, **kwargs):

        super().__init__()

        layout = QtWidgets.QHBoxLayout(self)
        button = QtWidgets.QPushButton('Center')
        button.clicked.connect(self.center)
        layout.addWidget(button)

    def center(self):
        qr = self.frameGeometry()
        cp = QtWidgets.QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    widget = CenteringWidget()
    widget.show()

    def custom_except_hook(t, val, tb):
        import traceback
        trace = ''.join(traceback.format_exception(t, val, tb))
        print(trace)
        QtCore.qFatal(str(val))

    sys.excepthook = custom_except_hook
    sys.exit(app.exec_())