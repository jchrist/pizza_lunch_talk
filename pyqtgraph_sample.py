from PyQt5 import QtGui, QtCore, QtWidgets
import numpy as np
import pyqtgraph as pg
from pyqtgraph.ptime import time
import sys


class SampleWidget(QtWidgets.QWidget):
    def __init__(self, **kwargs):

        super().__init__()

        self.canvas = pg.GraphicsLayoutWidget()
        layout = QtWidgets.QHBoxLayout(self)
        layout.addWidget(self.canvas)

        self.p = self.canvas.addPlot()
        self.p.setRange(QtCore.QRectF(0, -10, 5000, 20))
        self.p.setLabel('bottom', 'Index', units='B')
        self.curve = self.p.plot()

        self.data = np.random.normal(size=(50, 5000))
        self.ptr = 0
        self.lastTime = time()
        self.fps = None

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.update)
        self.timer.start(0)

    def update(self):
        self.curve.setData(self.data[self.ptr % 10])
        self.ptr += 1
        now = time()
        dt = now - self.lastTime
        self.lastTime = now
        if self.fps is None:
            self.fps = 1.0 / dt
        else:
            s = np.clip(dt * 3., 0, 1)
            self.fps = self.fps * (1 - s) + (1.0 / dt) * s
        self.p.setTitle('%0.2f fps' % self.fps)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    widget = SampleWidget()
    widget.show()

    def custom_except_hook(t, val, tb):
        import traceback
        trace = ''.join(traceback.format_exception(t, val, tb))
        print(trace)
        QtCore.qFatal(str(val))

    sys.excepthook = custom_except_hook
    sys.exit(app.exec_())
