from PyQt5 import QtWidgets, QtCore, QtGui
import sys


class CalculatorWidget(QtWidgets.QWidget):
    def __init__(self, *args, **kwargs):

        super(CalculatorWidget, self).__init__(*args, **kwargs)
        h_layout = QtWidgets.QVBoxLayout(self)
        self.line_edit = QtWidgets.QLineEdit()
        self.line_edit.setEnabled(False)
        self.line_edit.setStyleSheet('background: #FFFFFF; color: #000000')

        grid_layout = QtWidgets.QGridLayout()

        names = ['', '', 'Clear', 'Back',
                 '7', '8', '9', '/',
                 '4', '5', '6', '*',
                 '1', '2', '3', '-',
                 '0', '.', '=', '+']

        positions = [(i, j) for i in range(5) for j in range(4)]

        for position, name in zip(positions, names):

            if name == '':
                continue
            button = QtWidgets.QPushButton(name)
            button.clicked.connect(self.button_pressed)
            grid_layout.addWidget(button, *position)

        h_layout.addWidget(self.line_edit)
        h_layout.addLayout(grid_layout)

    def button_pressed(self) -> None:
        if self.sender().text() == '=':
            self.line_edit.setText(str(eval(self.line_edit.text())))
        elif self.sender().text() == 'Clear':
            self.line_edit.setText('')
        elif self.sender().text() == 'Back':
            self.line_edit.setText(self.line_edit.text()[:-1])
        else:
            self.line_edit.setText(self.line_edit.text() + self.sender().text())


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    widget = CalculatorWidget()
    widget.show()

    def custom_except_hook(t, val, tb):
        import traceback
        trace = ''.join(traceback.format_exception(t, val, tb))
        print(trace)
        QtCore.qFatal(str(val))

    sys.excepthook = custom_except_hook
    sys.exit(app.exec_())