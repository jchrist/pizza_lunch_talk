from PyQt5 import QtWidgets, QtCore, QtGui
import sys
from sample_widget import SampleWidget


class MainWindowWidget(QtWidgets.QMainWindow):
    def __init__(self, **kwargs):

        super(MainWindowWidget, self).__init__()

        self.setCentralWidget(SampleWidget())


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    widget = MainWindowWidget()
    widget.show()

    def custom_except_hook(t, val, tb):
        import traceback
        trace = ''.join(traceback.format_exception(t, val, tb))
        print(trace)
        QtCore.qFatal(str(val))

    sys.excepthook = custom_except_hook
    sys.exit(app.exec_())