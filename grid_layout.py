from PyQt5 import QtWidgets, QtCore
import sys


class CalculatorWidget(QtWidgets.QWidget):
    def __init__(self, *args, **kwargs):

        super(CalculatorWidget, self).__init__(*args, **kwargs)
        h_layout = QtWidgets.QVBoxLayout(self)
        self.label = QtWidgets.QLabel('I\'m a Label!')

        self.grid_layout = QtWidgets.QGridLayout()

        for i in range(5):
            for j in range(10):
                button = QtWidgets.QPushButton('{} {}'.format(i, j))
                button.clicked.connect(self.button_pressed)
                self.grid_layout.addWidget(button, i, j)

        h_layout.addWidget(self.label)
        h_layout.addLayout(self.grid_layout)

    def button_pressed(self) -> None:
        index = self.grid_layout.indexOf(self.sender())
        pos = self.grid_layout.getItemPosition(index)
        self.label.setText('{} {}'.format(pos[0], pos[1]))


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    widget = CalculatorWidget()
    widget.show()

    def custom_except_hook(t, val, tb):
        import traceback
        trace = ''.join(traceback.format_exception(t, val, tb))
        print(trace)
        QtCore.qFatal(str(val))

    sys.excepthook = custom_except_hook
    sys.exit(app.exec_())